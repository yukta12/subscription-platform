<?php

namespace App\Console\Commands;

use App\Jobs\SendPostMail;
use App\Models\Post;
use App\Models\SentNotification;
use App\Models\UserSubscribers;
use Illuminate\Console\Command;

class SendMailCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendEmail {postId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int {

        $post = Post::find($this->argument('postId'));
        $subscribers = UserSubscribers::where('website_id', $post->website_id)->with('users')->get();
        $input['subject'] = "A new post has been generated";

        //iterates over all the subscribers of that website and send mail
        foreach ($subscribers as $subscriber) {

            foreach ($subscriber->users as $user) {
                $doesntExist = SentNotification::where('user_id', $user->id)->where('post_id', $post->id)->doesntExist();

                //Checks if user has already received the email
                if ($doesntExist) {
                    $input['email'] = $user->email;
                    $input['name'] = $user->name;
                    \Mail::send('emails.test', ['postTitle'=>$post->title,'postBody'=>$post->body], function($message) use($input){
                        $message->to($input['email'], $input['name'])
                            ->subject($input['subject']);
                    });
                    SentNotification::create([
                        'post_id' => $post->id,
                        'user_id' => $user->id,
                        'created_by' => '1'
                    ]);
                    echo("mail sent!");
                }
            }
        }

        return Command::SUCCESS;
    }
}
