<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserSubscribers extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $table = 'user_subscribers';

    public function users(): HasMany {
        return $this->hasMany(User::class,'id');
    }
}
