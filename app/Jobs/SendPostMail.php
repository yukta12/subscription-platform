<?php

namespace App\Jobs;

use App\Models\Post;
use App\Models\SentNotification;
use App\Models\UserSubscribers;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendPostMail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $postTitle, $postBody, $websiteId;
    private $post;

    /**
     * SendPostMail constructor.
     * @param $postTitle
     * @param $postBody
     * @param $websiteId
     */
    public function __construct(Post $post) {
        $this->post = $post;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $data = UserSubscribers::where('website_id',$this->post->website_id)->with('users')->get();
        $input['subject'] = "A new post has been generated";

        foreach ($data as $key => $value) {
            foreach ($value->users as $user){
                $input['email'] = $user->email;
                $input['name'] = $user->name;

                //Checks if user has already received the email
                $doesntExist = SentNotification::where('user_id', $user->id) -> where('post_id', $this->post->id)->doesntExist();
                if ($doesntExist) {
                    \Mail::send('emails.test', ['postTitle'=>$this->post->title,'postBody'=>$this->post->body], function($message) use($input){
                        $message->to($input['email'], $input['name'])
                            ->subject($input['subject']);
                    });
                    SentNotification::create([
                        'post_id' => $this->post->id,
                        'user_id' => $user->id,
                        'created_by' => '1'
                    ]);
                }
            }

        }
    }
}
