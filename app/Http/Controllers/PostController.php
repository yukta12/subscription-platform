<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\constants\PostConstants;
use App\Jobs\SendPostMail;
use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class PostController extends Controller implements PostConstants {
    //
    /**
     * Creates a post and sends the mail as bg service
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws \Throwable
     */
    public function create(Request $request): JsonResponse {
        $validatedData = Validator::make($request->all(), self::CREATE_POST_VALIDATION);
        throw_if($validatedData->fails(), ValidationException::withMessages($validatedData->getMessageBag()->toArray()));
        $validatedData = $validatedData->validated();
        $validatedData['created_by'] = 1;
        $post = Post::create($validatedData);
        $job = (new SendPostMail($post))
            ->delay(
                now()
                    ->addSeconds(2)
            );
        dispatch($job);
        return ResponseHelper::success($post);

    }
}
