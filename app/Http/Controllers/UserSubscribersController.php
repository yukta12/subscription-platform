<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\User;
use App\Models\UserSubscribers;
use App\Models\Website;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserSubscribersController extends Controller {
    //
    public function create(User $user, Website $website): JsonResponse {
        $subscriberUser = UserSubscribers::where('user_id', $user->id)->where('website_id', $website->id)->first();
        if (is_null($subscriberUser)) {
            UserSubscribers::create(['user_id' => $user->id, 'website_id' => $website->id, 'created_by' => 1]);
            return ResponseHelper::success('', 'user subscribed succesfully');
        }
        return ResponseHelper::badRequest('already subscribed!');
    }
}
