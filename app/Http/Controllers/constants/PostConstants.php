<?php


namespace App\Http\Controllers\constants;


interface PostConstants {
    public const CREATE_POST_VALIDATION = [
        'title' => 'required|string|max:255|unique:posts',
        'body' => 'required',
        'website_id'=>'required|int|min:1|exists:websites,id'
    ];

}
