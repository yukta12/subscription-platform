<?php


namespace App\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class ResponseHelper {
    public const HTTP_TOKEN_EXPIRED             = 498;

    public const DEFAULT_SUCCESS_MESSAGE        = "Successfully fetched data";
    public const DEFAULT_INTERNAL_ERROR_FMT     = "Some internal server error occurred. %s";
    public const DEFAULT_FORBIDDEN_FMT          = "Cannot access the given route. %s";
    public const DEFAULT_SERVICE_UNAVAILABLE    = "Service is unavailable. %s";
    public const BAD_REQUEST_MESSAGE            = "Bad request";
    public const SUCCESSFULLY_CREATED_MESSAGE   = "Successfully Created";
    public const SUCCESSFULLY_DELETED_MESSAGE   = "Successfully Deleted";
    public const SUCCESSFULLY_UPDATED_MESSAGE   = "Successfully Updated";
    public const UNPROCESSABLE_ENTITY_MESSAGE   = "Could not process data";
    public const TOKEN_EXPIRED_MESSAGE          = "User's token expired";
    public const LOCKED_MESSAGE                 = "You already are logged in other device";


    public static function notFound(string $message) :JsonResponse {
        return response()->json([
            "message" => $message
        ], Response::HTTP_NOT_FOUND);
    }

    public static function internalError($details = null) :JsonResponse {
        return response()->json([
            "message" => sprintf(self::DEFAULT_INTERNAL_ERROR_FMT, $details)
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
    public static function serviceUnavailable($details = null) :JsonResponse {
        return response()->json([
            "message" => sprintf(self::DEFAULT_SERVICE_UNAVAILABLE, $details)
        ], Response::HTTP_SERVICE_UNAVAILABLE);
    }


    public static function forbidden($details = null) : JsonResponse {
        return response()->json([
            "message" => sprintf(self::DEFAULT_FORBIDDEN_FMT, $details)
        ], Response::HTTP_FORBIDDEN);
    }


    public static function locked($details = null) : JsonResponse {
        return response()->json([
            "message" => sprintf(self::LOCKED_MESSAGE, $details)
        ], Response::HTTP_LOCKED);
    }

    public static function badRequest($details = null, array $cookies = []) :JsonResponse {
        if ($details) {
            return self::withCookies(response()->json([
                "message"    => self::BAD_REQUEST_MESSAGE,
                "data"      => $details
            ], Response::HTTP_BAD_REQUEST), $cookies);
        }
        return self::withCookies(response()->json([
            "message"    => self::BAD_REQUEST_MESSAGE,
        ], Response::HTTP_BAD_REQUEST), $cookies);
    }

    public static function created($details = null, array $cookies=[]) : JsonResponse {
        return self::withCookies(response()->json([
            "message"    => self::SUCCESSFULLY_CREATED_MESSAGE,
            "data"       => $details
        ], Response::HTTP_CREATED), $cookies);
    }

    public static function deleted($details = null, array $cookies=[]) : JsonResponse {
        return self::withCookies(response()->json([
            "message"    => self::SUCCESSFULLY_DELETED_MESSAGE,
            "data"       => $details
        ], Response::HTTP_OK), $cookies);
    }

    public static function updated($details = null, array $cookies=[]) : JsonResponse {
        if ($details) {
            return self::withCookies(response()->json([
                "message"   => self::SUCCESSFULLY_UPDATED_MESSAGE,
                "data"      => $details
            ], Response::HTTP_OK), $cookies);
        }
        return self::withCookies(response()->json([
            "message"   => self::SUCCESSFULLY_UPDATED_MESSAGE,
        ], Response::HTTP_OK), $cookies);
    }

    public static function success($data = null, string $message = self::DEFAULT_SUCCESS_MESSAGE, array $cookies=[]) : JsonResponse {
        return self::withCookies(response()->json([
            "message"    => $message,
            "data"       => $data
        ], Response::HTTP_OK), $cookies);
    }

    public static function unprocessableEntity($data = null) : JsonResponse {
        return response()->json([
            "message"    =>  self::UNPROCESSABLE_ENTITY_MESSAGE,
            "data"      => $data
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public static function tokenExpired($data = null) : JsonResponse {
        return response()->json([
            "message"    =>  self::TOKEN_EXPIRED_MESSAGE,
            "data"      =>  $data
        ], self::HTTP_TOKEN_EXPIRED);
    }

    private static function withCookies(JsonResponse $response, array $cookies = []) : JsonResponse {
        foreach ($cookies as $cookie) {
            $response->withCookie($cookie);
        }
        return $response;
    }

    public static function isApiCall(Request $request) {
        return strpos($request->getUri(), '/api/') !== false;
    }
}
