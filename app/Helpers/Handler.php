<?php

namespace App\Helpers;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler {
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $exception) {
        if (app()->bound('sentry')) {
            app('sentry')->captureException($exception);
        }
        parent::report($exception);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register() {
        $this->renderable(function (ValidationException $exception, Request $request) {
            if (ResponseHelper::isApiCall($request)) {
                return ResponseHelper::badRequest($exception->errors());
            }
        });
        $this->renderable(function (\InvalidArgumentException $exception, Request $request) {
            if (ResponseHelper::isApiCall($request)) {
                return ResponseHelper::badRequest($exception->getMessage());
            }
        });
        $this->renderable(function (\ErrorException $exception, Request $request) {
            if (ResponseHelper::isApiCall($request)) {
                return ResponseHelper::internalError($exception->getMessage());
            }
        });
        $this->renderable(function (ModelNotFoundException $exception, Request $request) {
            if (ResponseHelper::isApiCall($request)) {
                return ResponseHelper::notFound($exception->getMessage());
            }
        });

    }
}
