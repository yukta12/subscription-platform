## Installation

Please check the official laravel installation guide for server requirements before you start


Clone the repository

    git clone git@gitlab.com:yukta12/subscription-platform.git

Switch to the repo folder

    cd subscription-platform

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate:fresh --seed

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000
add mailer to .env
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=61c8e3c6c4d42c
MAIL_PASSWORD=19d24d5b0294fd
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=yukta@gmail.com
MAIL_FROM_NAME="${APP_NAME}"

## API Specification

1. POST - /api/post/create  => creates the post that requires title, description and website_id
   ```angular2html 
   {
       "title":"some title goes here",
       "body":"body of the postgoes here"
       "website_id":1
   }
    ```

2. GET - /user/1/website/2/subscribe => subscribes the user to website


## command executing
php artisan command:sendEmail {postId} which is to be sent to the subscribers of that website who's post is

Example: php artisan command:sendEmail 2

