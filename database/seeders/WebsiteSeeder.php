<?php

namespace Database\Seeders;

use App\Models\Website;
use Illuminate\Database\Seeder;

class WebsiteSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $websites=[ 'https://gitlab.com/yukta12','https://github.com/yukta12','https://yuktapeswani.tk/'];
        foreach ($websites as $website){
            Website::create(['url'=>$website,'created_by'=>1]);
        }

    }
}
