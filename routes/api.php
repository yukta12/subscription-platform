<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\UserSubscribersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/',function (){
    return "hello";
});
Route::post('/post/create',[PostController::class,'create']);
Route::get('/user/{user}/website/{website}/subscribe',[UserSubscribersController::class,'create']);
