<!DOCTYPE html>
<html>
<head>
    <title>{{ $postTitle }}</title>
</head>
<body>
<h1>{{ $postTitle }}</h1>
<p>{{ $postBody }}</p>
</body>
</html>
